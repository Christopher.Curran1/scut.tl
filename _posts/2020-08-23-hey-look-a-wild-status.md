---
layout: post
title: Hey, look, a wild status has appeared
---

Lockdown really isn't the most conductive for me finishing things. I have started a lot of things, I just haven't finished many of them. It is all a trap. I am feeling pretty swamped at the moment while also being more relaxed and focused than I have been in a long time. I am developing new skills and reviving old ones but still feeling like I am just wasting time, an insane paradox.

I've been developing a streaming ML application which is why there are suddenly commits on my profile again. The stack is a bit insane, probably over-engineered. Kafka (mainly for playback), InfluxDB (dashboards, yo), python, scala, k3s and a few different APIs. The idea is to have the whole thing as async microservices that produce valid user feedback at the end. So far, so good. It is a bit of a complex setup but the pieces are simple to understand and functionally isolated which suits me from both a software engineering perspective and an ML perspective (different parts can be easily worked on in isolation and all parts stand-alone if needed). Watch this space.
