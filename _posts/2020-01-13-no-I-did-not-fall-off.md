---
layout: post
title: No I did not fall off the world
---

I had a pretty quiet weekend. Played games, played with my spawnlings and
started my new year fitness and diet regimen. What I didn't do was write any
code. I got pretty grumpy with it after Saturday morning when my cluster 
collapsed on AWS so lost the will to do much on a computer after that (it cost
me about 8 hours of processing time and some handholding of the code). Still
not pleased with it and also not pleased that AWS doesn't want to start my 
jobs again right now for unexplained, shit UI reasons but what can you do?

I'll be adding some new gists and repos soon. I have been doing some things but
not yet enough to fold into a single commit.