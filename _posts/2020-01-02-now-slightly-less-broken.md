---
layout: post
title: Now slightly less broken
---

When you are a developer or around software for a while you learn that
everything left unattended long enough will break into thousands of bits
Upgrades and updates are a constant. So here it is, somewhat updated.
