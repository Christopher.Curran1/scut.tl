---
layout: post
title: Captain's log
---

Stardate 2020-03-25, working from home by myself has been quiet. Very, very 
quiet. So quiet sometimes I have to tell myself to stop breathing so loud. It
really is strange, this forced isolation business. 

On the plus side, it has given me a lot of time to think. About what's important
and a lot about things that are really not important at all. Other than 
intellectually being pulled in many directions at once it has really made me sit
down (mostly standing) and think about what I really want to be doing. I have
compiled quiet the list, and culled a substantial amount more. I am focused now
on creation and on longer term goals (like being capable of pushing the FIRE
button in 2035). I now think I have some idea how I want to spend my hours and
the things I need to do to get there. I've also been back into music, and not 
only listening to it really loudly in the company of my own study but on 
producing it too. I am feeling creative and strong. I have come closer to 
getting to my personal fitness goals (at least my short term goals) and I am
feeling great. Now, I need to focus on my intellectual and financial goals.
Take active steps to get to where I want to be and doing the things I want to do
to get there. I feel very driven.
