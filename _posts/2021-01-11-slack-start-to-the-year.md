---
layout: post
title: Slack start to the year
---

Well 2021 is definitely more of the same as 2020 so far. I haven't been the most active in posting but I've been having ideas about actually making some content. Amazing, I know.

Another lockdown has fried my brain. The complete opposite of being productive, it destroyed my ability to focus for almost two days. Sometimes constraint is the mother of invention but not this time. Anyway, I'm trying to commit myself to more making, more creation and constantly be doing rather than watching. We'll see how long it lasts but disconnecting myself from my phone is the first step.


