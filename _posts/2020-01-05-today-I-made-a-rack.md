---
layout: post
title: Today I made a rack
---

I have a tone of different computer parts and side projects kicking around 
that I really never fully finish. Today I finished stacking my lack rack with
a [raspberrypi rack](https://www.thingiverse.com/thing:2705742) and lots more
network cables. Slowly, I am building up a very formidable homelab.
