---
layout: post
title: Reverse New Years manifesto
---

The path most people take is to make their New Years resolutions and then fail to keep them in 
February. I'm going to try and go the other way this time and start doing them now and continue
once I get to January. Today it is ensuring I have committed my code and written more than a 
text to a rando. Also Advent of Code again. I need to up my leetcode game to prove I am still 
down with the kids in startup land.

Peace out.