---
layout: post
title: Stop touching me
---

I never really said it had to be a good commit, just a commit. It is habit 
forming. There is other code I've been working on but I'm not ready to release
into the wild yet though.