---
layout: post
title: Touch everything
---

I am going to stick to doing at least one commit today, even if I am insanely
busy as I am today. This post is the equivalent of me touching a file while my
code is chugging away on EMR.

[GNU parallel](https://www.gnu.org/software/parallel/) 
is a great tool, learn it if you don't already know it. It is like `tee` and 
`xargs` except it makes rocket go fast and if you can cap 100% cpu on a single
core for a script then it can be the laziest path to scaling a process ever.